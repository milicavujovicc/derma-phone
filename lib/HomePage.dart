import 'package:derma_phone/WelcomePage.dart';
import 'package:derma_phone/screens/ArticlesPage.dart';
import 'package:derma_phone/screens/MessagesPage.dart';
import 'package:derma_phone/screens/ProfilePage.dart';
import 'package:derma_phone/screens/RequestsPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 4, initialIndex: 0, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        //title: new Text("Derma Phone", style: TextStyle(fontFamily: 'Serif')),
        title: SizedBox(
          child: Image.asset("assets/dermaphone.png", color: Colors.white, fit: BoxFit.fitHeight),
        ),
        backgroundColor: Colors.teal,
        foregroundColor: Colors.tealAccent,
        elevation: 0.7,
        bottom: new TabBar(
          controller: _tabController,
          indicatorColor: Colors.tealAccent,
          labelColor: Colors.white,
          tabs: <Widget>[
            new Tab(icon: new Icon(Icons.home_outlined)),
            new Tab(icon: new Icon(Icons.list_alt_outlined)),
            new Tab(icon: new Icon(Icons.message_outlined)),
            new Tab(icon: new Icon(Icons.account_circle_outlined))
          ]
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => WelcomePage()),
              );
            },
          )
        ]
      ),
      body: new TabBarView(
        controller: _tabController,
        children:  <Widget>[
          new ArticlesPage(),
          new RequestsPage(),
          new MessagesPage(),
          new ProfilePage(),
        ],
      ),
    );
  }
}