import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Utility {

  static void showMessage({BuildContext context, String text, bool error = false}) {
    ScaffoldMessenger.of(context)
        .showSnackBar(
        SnackBar(
          content: Text(text),
          backgroundColor: (error) ? Colors.redAccent : Colors.grey,
          duration: Duration(seconds: 1),
        ));
  }
}