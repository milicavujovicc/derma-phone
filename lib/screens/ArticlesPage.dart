import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'ArticlePage.dart';
import 'package:derma_phone/models/Article.dart';
import 'package:derma_phone/repositories/ArticlesRepo.dart';

class ArticlesPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(left: 25, right: 25, top: 19, bottom: 19),
        child: ListView.builder(
          padding: EdgeInsets.only(left: 10, right: 10),
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (context, index) {
            return _buildArticleElement(context: context, article: ArticlesRepo.getInstance().articles[index]);
          },
        )
      )
    );
  }

  Widget _buildArticleElement({BuildContext context, Article article}) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ArticlePage(article: article)),
        );
      },
      child: Padding(
        padding: EdgeInsets.only(top: 20),
        child: Card(
          child: Column(
            children: [
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.only(bottom: 10),
                height: 250,
                width: 250,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  image: DecorationImage(
                    image: AssetImage("assets/" + article.id.toString() + ".jpg"),
                  )
                )
              ),
              SizedBox(height: 10),
              Text(article.naziv,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                  fontFamily: 'Serif',
                  fontWeight: FontWeight.bold
                )
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: Text(article.opis,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.black45,
                        fontFamily: 'Serif'
                    )
                ),
              ),
              SizedBox(height: 20)
            ]
          )
        )
      )
    );
  }

}