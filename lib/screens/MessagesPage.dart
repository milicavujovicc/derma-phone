import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:derma_phone/repositories/MessagesRepo.dart';
import 'NewMessagePage.dart';

class MessagesPage extends StatefulWidget {

  @override
  MessageState createState() {
    return new MessageState();
  }

}

class MessageState extends State<MessagesPage> {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: MessagesRepo.getInstance().messages.length,
      itemBuilder: (context, i) => new Column(
        children: <Widget> [
          new Divider(
            height: 10.0,
          ),
          new ListTile(
            leading: new CircleAvatar(
              foregroundColor: Colors.tealAccent,
              backgroundColor: Colors.grey,
              backgroundImage: new AssetImage("assets/" + MessagesRepo.getInstance().messages[i].lekar.ime + ".jpg"),
            ),
            title: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text(
                  MessagesRepo.getInstance().messages[i].lekar.ime,
                  style: new TextStyle(fontWeight: FontWeight.bold)
                ),
                new Text(
                  MessagesRepo.getInstance().messages[i].vreme,
                  style: new TextStyle(color: Colors.grey, fontSize: 14.0)
                )
              ]
            ),
            subtitle: new Container(
              padding: const EdgeInsets.only(top: 5.0),
              child: new Text(
                MessagesRepo.getInstance().messages[i].poruka,
                style: new TextStyle(color: Colors.grey, fontSize: 15.0)
              )
            ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NewMessagePage(doctor: MessagesRepo.getInstance().messages[i].lekar))
                );
              }
          )
        ]
      ),
    );
  }
}