import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:derma_phone/models/Article.dart';

class ArticlePage extends StatefulWidget {
  final Article article;

  ArticlePage({this.article});

  @override
  _ArticlePageState createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Colors.teal,
                expandedHeight: MediaQuery.of(context).size.height*0.5,
                flexibleSpace: Container(
                  height: MediaQuery.of(context).size.height*0.5,
                  child: Stack(
                    children: <Widget>[
                      FlexibleSpaceBar(
                        background: Center(
                          child: Container(
                            alignment: Alignment.bottomCenter,
                            width: 300,
                            height: 300,
                            //margin: EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              image: DecorationImage(
                                image: AssetImage("assets/" + widget.article.id.toString() + ".jpg"),
                              )
                            )
                          )
                        )
                      )
                    ]
                  )
                ),
              ),
              SliverList(delegate: SliverChildListDelegate(
                [
                  Padding(
                    padding: EdgeInsets.only(top: 24, left: 25),
                    child: Text(widget.article.naziv, style: TextStyle(fontFamily: 'Serif', fontSize: 22, color: Colors.black87, fontWeight: FontWeight.w400))
                  ),
                  Container(
                    height: 28,
                    margin: EdgeInsets.only(top: 23, bottom: 10),
                    padding: EdgeInsets.only(left: 25),
                    child: DefaultTabController(
                      length: 1,
                      child: TabBar(
                        labelPadding: EdgeInsets.all(0),
                        indicatorPadding: EdgeInsets.all(0),
                        isScrollable: true,
                        labelColor: Colors.black87,
                        unselectedLabelColor: Colors.black45,
                        /*
                        onTap: (tabIndex) {
                          setState(() {
                            switch(tabIndex) {
                              case 0: textToShow = widget.article.opis; break;
                            }
                          });
                        },
                        */
                        labelStyle: TextStyle(fontFamily: 'Serif', fontSize: 14, fontWeight: FontWeight.w600),
                        unselectedLabelStyle: TextStyle(fontFamily: 'Serif', fontSize: 14, fontWeight: FontWeight.w500),
                        indicator: UnderlineTabIndicator(
                          borderSide: BorderSide(width: 2.0, color: Colors.tealAccent),
                          insets: EdgeInsets.fromLTRB(0, 0, 35, 0),
                        ),
                        tabs: [
                          Tab(
                            child: Container(
                              margin: EdgeInsets.only(right: 39),
                              child: Text('Opis')
                            )
                          )
                        ]
                      )
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 25, right: 25, bottom: 25),
                    child: Text(widget.article.opisDug,
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Colors.black54,
                        letterSpacing: 1.5,
                        height: 2,
                      ),
                    )
                  ),
                  SizedBox(height: 10.0)
                ]
              ))
            ]
          )
        )
      )
    );
  }

  //String textToShow = widget.article.opis;
}