import 'dart:io';
import 'package:derma_phone/models/Message.dart';
import 'package:derma_phone/repositories/MessagesRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:derma_phone/models/Request.dart';
import 'package:image_picker/image_picker.dart';

class NewMessagePage extends StatefulWidget {
  final Request doctor;

  NewMessagePage({this.doctor});

  @override
  NewMessageState createState() {
    return new NewMessageState();
  }
}

class NewMessageState extends State<NewMessagePage> {
  TextEditingController textEditingController = TextEditingController();
  List<Message> chatMessages;
  ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    chatMessages =
        MessagesRepo.getInstance().getChatMessages(widget.doctor.ime);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        //automaticallyImplyLeading: false,
        title: new Text(widget.doctor.ime,
            style: TextStyle(fontFamily: 'Serif', color: Colors.white)),
        backgroundColor: Colors.teal,
        foregroundColor: Colors.tealAccent,
        elevation: 0.7,
      ),
      body: ListView.builder(
        itemCount: chatMessages.length,
        itemBuilder: (context, index) {
          return _buildMessageWidget(message: chatMessages.elementAt(index));
        },
      ),
      bottomSheet: new Row(children: <Widget>[
        Expanded(
            child: Padding(
                padding: EdgeInsets.fromLTRB(11, 0, 11, 7),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () async {
                        final pickedFile = await _picker.pickImage(
                            source: ImageSource.gallery,
                            maxHeight: 150,
                            maxWidth: 150);
                        _sendNewImageMessage(pickedFile);
                      },
                      child: Icon(Icons.camera_alt_outlined,
                          color: Colors.teal[400], size: 24),
                    ),
                    SizedBox(width: 15),
                    Expanded(
                      child: TextField(
                        controller: textEditingController,
                        decoration: InputDecoration(
                            hintText: "Napiši poruku...",
                            hintStyle: TextStyle(
                                color: Colors.black54,
                                fontSize: 16,
                                fontFamily: 'Serif'),
                            border: InputBorder.none),
                      ),
                    ),
                    SizedBox(width: 15),
                    GestureDetector(
                      onTap: () {
                        _sendNewMessage();
                      },
                      child: Icon(Icons.send_rounded,
                          color: Colors.teal[400], size: 24),
                    ),
                  ],
                ))),
      ]),
    );
  }

  Widget _buildMessageWidget({Message message}) {
    bool sentByDoctor = (message.lekar != null);

    if (message.image == null) {
      // text message
      return Align(
        alignment:
            (sentByDoctor) ? Alignment.centerLeft : Alignment.centerRight,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: (sentByDoctor) ? Colors.grey[300] : Colors.teal[100],
            ),
            child: Text(message.poruka + "\n" + message.vreme,
                textAlign: TextAlign.end,
                style: TextStyle(fontSize: 15, fontFamily: 'Serif')),
          ),
        ),
      );
    } else {
      // image message
      return Align(
        alignment:
            (sentByDoctor) ? Alignment.centerLeft : Alignment.centerRight,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: (sentByDoctor) ? Colors.grey[300] : Colors.teal[100],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Image.file(File(message.image.path)),
                  Text(message.vreme,
                      textAlign: TextAlign.end,
                      style: TextStyle(fontSize: 15, fontFamily: 'Serif')),
                ],
              )),
        ),
      );
    }
  }

  void _sendNewMessage() {
    String messageText = textEditingController.value.text;
    String messageTime = DateTime.now().hour.toString() +
        ":" +
        DateTime.now().minute.toString().padLeft(2, "0");

    setState(() {
      chatMessages.add(Message(
          lekar: null, // sent by me
          poruka: messageText,
          vreme: messageTime));
    });

    String telegramMessage = "Doctor " +
        widget.doctor.ime +
        " received a new message: " +
        messageText;
    MessagesRepo.getInstance().sendTelegramMessage(message: telegramMessage);
    textEditingController.clear();
  }

  void _sendNewImageMessage(XFile image) {
    String messageText = "";
    String messageTime = DateTime.now().hour.toString() +
        ":" +
        DateTime.now().minute.toString().padLeft(2, "0");

    setState(() {
      chatMessages.add(Message(
          lekar: null, // sent by me
          poruka: messageText,
          vreme: messageTime,
          image: image));
    });

    MessagesRepo.getInstance().sendTelegramMessage();
    textEditingController.clear();
  }
}
