import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:derma_phone/repositories/ProfileRepo.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController imePrezimeController = TextEditingController(text: ProfileRepo.getInstance().imePrezime);
  TextEditingController korImeController = TextEditingController(text: ProfileRepo.getInstance().korisnickoIme);

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Stack(
            children: [
              Container(
                color: Colors.teal,
                width: 410,
                height: 100,
              ),
              Container(
                color: Colors.white,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text("Izmenite svoje lične podatke",
                        style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic, fontFamily: 'Serif', color: Colors.teal),
                      ),
                      SizedBox(
                          height: 22.0
                      ),
                      TextField(
                        autofocus: true,
                        controller: imePrezimeController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                          labelText: "Ime i Prezime",
                          prefixIcon: Icon(
                            Icons.person,
                            color: Colors.teal[400],
                          ),
                          labelStyle: TextStyle(color: Colors.black87),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                        ),
                      ),
                      SizedBox(
                          height: 7.0
                      ),
                      TextField(
                        autofocus: true,
                        readOnly: true,
                        controller: korImeController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                          labelText: "Korisničko ime",
                          prefixIcon: Icon(
                            Icons.account_circle_rounded,
                            color: Colors.teal[400],
                          ),
                          labelStyle: TextStyle(color: Colors.black87),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                        ),
                      ),
                      SizedBox(
                          height: 7.0
                      ),
                      TextField(
                        autofocus: true,
                        obscureText: true,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.black45)),
                          labelText: "Nova lozinka",
                          hintText: "Vaša lozinka",
                          prefixIcon: Icon(
                            Icons.lock_outlined,
                            color: Colors.teal[400],
                          ),
                          labelStyle: TextStyle(color: Colors.black87),
                          hintStyle: TextStyle(color: Colors.black45),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                        ),
                      ),
                      SizedBox(
                          height: 7.0
                      ),
                      TextField(
                        autofocus: true,
                        obscureText: true,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.black45)),
                          labelText: "Potvrda lozinke",
                          hintText: "Potvrdite vašu lozinku",
                          prefixIcon: Icon(
                            Icons.lock,
                            color: Colors.teal[400],
                          ),
                          labelStyle: TextStyle(color: Colors.black87),
                          hintStyle: TextStyle(color: Colors.black45),
                          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                        ),
                      ),
                      SizedBox(
                          height: 15.0
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.teal, shape: StadiumBorder()),
                        onPressed: () {
                          ProfileRepo.getInstance().imePrezime = imePrezimeController.value.text;
                          ProfileRepo.getInstance().korisnickoIme = korImeController.value.text;
                          setState(() {

                          });
                        },
                        child: const Text('Potvrdi', style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Serif')),
                      ),
                      SizedBox(
                          height: 7.0
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.teal[300], shape: StadiumBorder()),
                        onPressed: () {
                          setState(() {
                            // just clear data;
                            imePrezimeController = TextEditingController(text: ProfileRepo.getInstance().imePrezime);
                            korImeController = TextEditingController(text: ProfileRepo.getInstance().korisnickoIme);
                          });
                        },
                        child: const Text('Odustani', style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Serif')),
                      ),
                      SizedBox(
                        height: 200.0
                      )
                    ],
                  ),
                ),
                padding: EdgeInsets.all(20),
              )
            ]
        )
    );
  }
}