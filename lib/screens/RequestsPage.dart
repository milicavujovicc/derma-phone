import 'package:derma_phone/screens/NewMessagePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:derma_phone/models/Request.dart';
import 'package:derma_phone/repositories/RequestsRepo.dart';

class RequestsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(left: 25, right: 25, top: 19, bottom: 19),
        child: ListView.builder(
          padding: EdgeInsets.only(left: 10, right: 10),
          itemCount: RequestsRepo.getInstance().requests.length,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          reverse: false,
          itemBuilder: (context, index) {
            return _buildRequestElement(context: context, request: RequestsRepo.getInstance().requests[index]);
          },
        )
      )
    );
  }

  Widget _buildRequestElement({BuildContext context, Request request}) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: 10),
        child: Card(
          color: Colors.teal[100],
          shadowColor: Colors.teal[200],
          child: Column(
            children: [
              SizedBox(height:10),
              Container(
                  alignment: Alignment.bottomCenter,
                  width: 300,
                  height: 300,
                  //margin: EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      image: DecorationImage(
                        image: AssetImage("assets/" + request.ime + ".jpg"),
                      )
                  )
              ),
              SizedBox(height: 10),
              Text("Dr. " + request.ime,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 22,
                  color: Colors.black,
                  fontFamily: 'Serif',
                  fontWeight: FontWeight.bold
                )
              ),
              SizedBox(height: 10),
              Text(request.opis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.black45,
                      fontFamily: 'Serif',
                      fontWeight: FontWeight.w300
                  )
              ),
              SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: const Icon(Icons.message_rounded),
                    color: Colors.teal[400],
                    hoverColor: Colors.teal[600],
                    iconSize: 30,
                    tooltip: "Send a message to " + request.ime,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => NewMessagePage(doctor: request))
                      );
                    }
                  ),
                  SizedBox(width: 10),
                  Icon(
                    Icons.circle,
                    size: 19,
                    color:request.status?Colors.green[500]:Colors.grey[600]
                  ),
                  SizedBox(width: 5),
                  Text(request.status?"Online":"Offline",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.black54,
                          fontFamily: 'Serif',
                          fontWeight: FontWeight.w500
                      )
                  ),
                ]
              )
            ]
          )
        )
      )
    );
  }
}