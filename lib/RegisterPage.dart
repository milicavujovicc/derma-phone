import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'LoginPage.dart';

class RegisterPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Stack(
            children: [
              Container(
                color: Colors.teal,
                width: 410,
                height: 100,
              ),
              Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                        height: 30.0
                    ),
                    Image.asset("assets/dermaphone.png",
                      fit: BoxFit.cover,
                      color: Colors.teal,
                    ),
                    SizedBox(
                        height: 15.0
                    ),
                    Text("Unesite svoje lične podatke",
                      style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic, fontFamily: 'Serif', color: Colors.teal),
                    ),
                    SizedBox(
                        height: 20.0
                    ),
                    TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                        labelText: "Ime i prezime",
                        hintText: "Mika Mikic",
                        prefixIcon: Icon(
                          Icons.person,
                          color: Colors.teal[400],
                        ),
                        labelStyle: TextStyle(color: Colors.black45),
                        hintStyle: TextStyle(color: Colors.black26),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                      ),
                    ),
                    SizedBox(
                        height: 7.0
                    ),
                    TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                        labelText: "Korisničko ime",
                        hintText: "mika_mikic",
                        prefixIcon: Icon(
                          Icons.account_circle_rounded,
                          color: Colors.teal[400],
                        ),
                        labelStyle: TextStyle(color: Colors.black45),
                        hintStyle: TextStyle(color: Colors.black26),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                      ),
                    ),
                    SizedBox(
                        height: 7.0
                    ),
                    TextField(
                      autofocus: true,
                      obscureText: true,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.black45)),
                        labelText: "Lozinka",
                        hintText: "Vaša lozinka",
                        prefixIcon: Icon(
                          Icons.lock_outlined,
                          color: Colors.teal[400],
                        ),
                        labelStyle: TextStyle(color: Colors.black45),
                        hintStyle: TextStyle(color: Colors.black26),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                      ),
                    ),
                    SizedBox(
                        height: 7.0
                    ),
                    TextField(
                      autofocus: true,
                      obscureText: true,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.black45)),
                        labelText: "Potvrda lozinke",
                        hintText: "Potvrdite vašu lozinku",
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.teal[400],
                        ),
                        labelStyle: TextStyle(color: Colors.black45),
                        hintStyle: TextStyle(color: Colors.black26),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                      ),
                    ),
                    SizedBox(
                        height: 15.0
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.teal, shape: StadiumBorder()),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()),
                        );
                      },
                      child: const Text('Potvrdi', style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Serif')),
                    ),
                    SizedBox(
                        height: 15.0
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.teal[300], shape: StadiumBorder()),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Odustani', style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Serif')),
                    ),
                    SizedBox(
                        height: 15.0
                    ),
                    Image.asset("assets/logo.png",
                      fit: BoxFit.cover,
                      width: 70,
                      height: 70,
                    ),
                  ],
                ),
                padding: EdgeInsets.all(20),
              )
            ]
        )
    );
  }

}