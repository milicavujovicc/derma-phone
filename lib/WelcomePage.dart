import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'LoginPage.dart';
import 'RegisterPage.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build (BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Container(
            color: Colors.white,
            ),
          Container(
            decoration: BoxDecoration(
              color: Colors.teal,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(60),
                bottomLeft: Radius.circular(60),
              )
            ),
            width: 400,
            height: 540,
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                SizedBox(height: 30.0),
                Image.asset("assets/dermaphone.png",
                  fit: BoxFit.cover,
                  width: 300,
                ),
                SizedBox(height: 25.0),
                Image.asset("assets/logo.png",
                  fit: BoxFit.cover,
                  width: 250,
                  height: 250,
                ),
                SizedBox(height: 25.0),
                Text("Dobrodošli na\nDerma Phone App!",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, fontFamily: 'Serif', color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ]
            )
          ),
          Container(
            margin: EdgeInsets.all(55),
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.teal[300], shape: StadiumBorder(), padding: EdgeInsets.fromLTRB(20, 10, 20, 10)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                  child: const Text('Uloguj se', style: TextStyle(fontSize: 25, color: Colors.white, fontFamily: 'Serif')),
                ),
                SizedBox(height: 19),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.teal[300], shape: StadiumBorder(), padding: EdgeInsets.fromLTRB(20, 10, 20, 10)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegisterPage()),
                    );
                  },
                  child: const Text('Registruj se', style: TextStyle(fontSize: 25, color: Colors.white, fontFamily: 'Serif')),
                ),
              ]
            )
          )
        ]
      )
    );
  }
}