class Article {
  int id;
  String naziv;
  String opis;
  String opisDug;

  Article({this.id, this.naziv, this.opis, this.opisDug});
}