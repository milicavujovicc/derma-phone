import 'package:image_picker/image_picker.dart';

import 'Request.dart';

class Message {
  Request lekar;
  String poruka;
  String vreme;
  XFile image;

  Message({this.lekar, this.poruka, this.vreme, this.image});
}