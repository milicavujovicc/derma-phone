import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'HomePage.dart';
import 'package:derma_phone/util/utility.dart';

class LoginPage extends StatelessWidget {
  TextEditingController _passwordController = TextEditingController();
  TextEditingController usernameTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        child: Stack(
          children: [
            Container(
              color: Colors.brown,
              width: 410,
              height: 100,
            ),
            Container(
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 30.0
                  ),
                  Image.asset("assets/dermaphone.png",
                    fit: BoxFit.cover,
                    color: Colors.teal,
                  ),
                  SizedBox(
                      height: 15.0
                  ),
                  Text("Unesite svoje kredencijale",
                    style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic, fontFamily: 'Serif', color: Colors.teal),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextField(
                    autofocus: true,
                    controller: usernameTextController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                      labelText: "Korisničko ime",
                      hintText: "mika_mikic",
                      prefixIcon: Icon(
                        Icons.email,
                        color: Colors.teal[400],
                      ),
                      labelStyle: TextStyle(color: Colors.black45),
                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                    ),
                  ),
                  SizedBox(
                      height: 20.0
                  ),
                  TextField(
                    autofocus: true,
                    obscureText: true,
                    controller: _passwordController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.black45)),
                      labelText: "Lozinka",
                      hintText: "Vaša lozinka",
                      prefixIcon: Icon(
                        Icons.lock,
                        color: Colors.teal[400],
                      ),
                      labelStyle: TextStyle(color: Colors.black45),
                      focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0), borderSide: BorderSide(color: Colors.teal[400])),
                    ),
                  ),
                  SizedBox(
                      height: 20.0
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.teal, shape: StadiumBorder()),
                    onPressed: () {
                      String username = usernameTextController.value.text;
                      if (username == "mika") {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => HomePage()),
                        );
                      } else {
                        Utility.showMessage(context: context, text: "Ne postoji korisnik", error: true);
                      }
                    },
                    child: const Text('Potvrdi', style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Serif')),
                  ),
                  SizedBox(
                      height: 15.0
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.teal[300], shape: StadiumBorder()),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Odustani', style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Serif')),
                  ),
                  SizedBox(
                      height: 15.0
                  ),
                  Image.asset("assets/logo.png",
                    fit: BoxFit.cover,
                    width: 70,
                    height: 70,
                  ),
                ]
              ),
              padding: EdgeInsets.all(20),
            )
          ]
        )
      )
    );
  }
}