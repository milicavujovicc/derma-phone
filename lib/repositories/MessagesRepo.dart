import 'package:derma_phone/models/Message.dart';
import 'package:teledart/telegram.dart';
import 'RequestsRepo.dart';

class MessagesRepo {
  static MessagesRepo _instance;

  String telegramToken = "1912154652:AAHvT3AEl-XeGf60cA64ZWUllRykz46FZh0";
  String myChatId = "433947657";

  MessagesRepo._internal();

  static MessagesRepo getInstance() {
    if (_instance == null) {
      _instance = MessagesRepo._internal();
    }
    return _instance;
  }

  List<Message> messages = [
    Message(lekar: RequestsRepo.getInstance().requests[1], poruka:"Nema na čemu.", vreme:"15:30"),
    Message(lekar: RequestsRepo.getInstance().requests[2], poruka:"Bolje. Hvala na pitanju!", vreme:"11:30")
  ];

  List<Message> getChatMessages(String doctorName) {

    if (doctorName.startsWith("Marko")) {
      return [
        Message(lekar: RequestsRepo.getInstance().requests[1], poruka:"Taj antibiotik će Vam pomoći.", vreme:"15:30"),
        Message(lekar: null, poruka:"U redu. Hvala, Mare.", vreme:"15:31"),
        Message(lekar: RequestsRepo.getInstance().requests[1], poruka:"Nema na čemu.", vreme:"15:33")
      ];
    } else if (doctorName.startsWith("Jovana")) {
      return [
        Message(lekar: RequestsRepo.getInstance().requests[2], poruka:"Kako ste danas?", vreme:"11:25"),
        Message(lekar: null, poruka:"Bolje. Hvala na pitanju!", vreme:"11:30")
      ];
    }

    return [];
  }

  void sendTelegramMessage({String message = "You have a new message!"}) {
    var telegram = Telegram(telegramToken);
    telegram.sendMessage(myChatId, message);
  }

}