import 'package:derma_phone/models/Request.dart';

class RequestsRepo {
  static RequestsRepo _instance;
  static const String ONOF_STATUS = "Offline";

  RequestsRepo._internal();

  static RequestsRepo getInstance() {
    if (_instance == null) {
      _instance = RequestsRepo._internal();
    }
    return _instance;
  }

  List<Request> requests = [
    Request(ime:"Tijana Perišić", opis:"Specijalista dermatovenerologije", status:false, broj:"+381648212000"),
    Request(ime:"Marko Sarić", opis:"Dermatovenerolog", status:false, broj:"+381648212000"),
    Request(ime:"Jovana Mihajlović", opis:"Specijalista dermatologije", status:false, broj:"+381648212000")
  ];
}