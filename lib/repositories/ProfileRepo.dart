class ProfileRepo {
  static ProfileRepo _instance;

  String imePrezime = "Mika Mikic";
  String korisnickoIme = "mika";

  ProfileRepo._internal();

  static ProfileRepo getInstance() {
    if (_instance == null) {
      _instance = ProfileRepo._internal();
    }
    return _instance;
  }
}